import { Injectable } from '@angular/core';
import { Message } from './message.model';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, Subject, Subscription } from 'rxjs';
import { map, tap } from 'rxjs/operators';

@Injectable()
export class MessagesService {
  messagesChange = new Subject<Message[]>();
  messagesFetching = new Subject<boolean>();
  messagesUploading = new Subject<boolean>();
  observableOfFirstMessagesSubscription!: Subscription;
  observableOfNewMessagesSubscription!: Subscription;
  date = '';
  stopInterval = false;

  private messagesArray: Message[] = [];

  constructor(private http: HttpClient) {}

  fetchMessages() {
    const observableOfFirstMessages = new Observable<Message[]>(subscriber => {
      this.messagesFetching.next(true);
      this.http.get<Message[]>('http://146.185.154.90:8000/messages')
        .pipe(map(result => {
          if (result.length === 0) {
            return [];
          }
          return result.map(message => {
            return new Message(message.id, message.message, message.author, message.datetime);
          });
        }))
        .subscribe(messages => {
          subscriber.next(this.messagesArray = messages);
        }, () => {
          subscriber.error(new Error('Something has gone wrong.'));
        }, () => {
          subscriber.complete();
        });
    });
    this.observableOfFirstMessagesSubscription = observableOfFirstMessages.subscribe(() => {
      this.messagesChange.next(this.messagesArray.slice());
      this.date = this.messagesArray[this.messagesArray.length - 1].datetime;
      this.messagesFetching.next(false);
    }, (error) => {
      console.log(error.message);
      this.messagesFetching.next(false);
    }, () => {
      console.log('Downloading of 30 messages is completed');
    });
  }

  addMessage(message: Message) {
    const body = new HttpParams()
      .set('author', message.author)
      .set('message', message.message);
    this.messagesUploading.next(true);
    return this.http.post('http://146.185.154.90:8000/messages', body)
      .pipe(tap(() => {
        this.messagesUploading.next(false);
      }, () => {
        this.messagesUploading.next(false);
      }));
  }

  start() {
    const observableOfNewMessages = new Observable<Message[] | []>(subscriber => {
      let interval = 0;
      if (interval) return;
      interval = setInterval(() => {
        if (this.stopInterval) {
         return  subscriber.complete();
        }
        this.http.get<Message[] | []>(`http://146.185.154.90:8000/messages?datetime=${this.date}`)
          .pipe(map(result => {
            if (result.length === 0) {
              return [];
            }
            return result.map(message => {
              return new Message(message.id, message.message, message.author, message.datetime);
            });
          }))
          .subscribe(newMessages => {
            this.messagesArray = this.messagesArray.concat(newMessages);
            subscriber.next(this.messagesArray);
          }, () => {
            subscriber.error(new Error('Something has gone wrong with new messages in chat.'));
          });
      }, 2000);
      return {
        unsubscribe() {
          clearInterval(interval);
          interval = 0;
        }
      }
    });
    this.observableOfNewMessagesSubscription = observableOfNewMessages.subscribe(() => {
      this.date = this.messagesArray[this.messagesArray.length - 1].datetime;
      this.messagesChange.next(this.messagesArray.slice());
    }, (error) => {
      console.log(error.message);
    });
  }

  stop() {
    this.stopInterval = true;
    this.observableOfNewMessagesSubscription.unsubscribe();
  }
}
